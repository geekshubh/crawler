<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('analysis',['uses'=>'AnalysisController@analyse','as'=>'analysis.index']);
Route::post('users/store',['uses'=>'UserController@store','as'=>'user.store']);
Route::get('users/{id}',['uses'=>'UserController@view','as'=>'user.view']);
Route::put('users/{id}/update',['uses'=>'UserController@update','as'=>'user.update']);
