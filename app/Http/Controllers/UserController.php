<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{
	public function store(Request $request)
	{
		$data =  $request->json()->all();
		$check_email = User::where('email',$data['email'])->count();
		if($check_email == '0')
		{	$user = new User;
			$user->name = $data['name'];
			$user->email = $data['email'];
			$user->profile_pic = $data['profile_pic'];
			$user->content_creator = $data['content_creator'];
			$user->content_enhancer = $data['content_enhancer'];
			$user->content_consumer = $data['content_consumer'];
			$user->save();
			return $user;
		}
		else
		{
			$error = 'email already exists';
			return  $error;
		}
	}
	public function view($id)
	{
		$user = User::where('_id',$id)->get();
		return $user;
	}
	public function update(Request $request,$id)
	{
		$user = User::find($id);
		$data =  $request->json()->all();
		if(!empty($data['name']))
		{
			$user->name = $data['name'];
		}
		if(!empty($data['email']))
		{
			$user->email = $data['email'];
		}
		if(!empty($data['profile_pic']))
		{
			$user->profile_pic = $data['profile_pic'];
		}
		if(!empty($data['content_creator']))
                {
                        $user->content_creator = $data['content_creator'];
                }
		if(!empty($data['content_enhancer']))
                {
                        $user->content_enhancer = $data['content_enhancer'];
                }
		if(!empty($data['content_consumer']))
                {
                        $user->content_consumer = $data['content_consumer'];
                }
		$user->update();
		return $user;
	}
}
