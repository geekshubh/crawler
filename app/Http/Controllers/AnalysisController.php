<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Sentiment\Analyzer;
use App\Article;
class AnalysisController extends Controller
{
  public function analyse(Request $request)
  {
    $data =  $request->json()->all();
    $request_url = $data['url'];
    $url = preg_replace("(^https?://)", "", $request_url);
    $existing_article = Article::select('title','url','image_url','sentiment_analysis')->where('url',$url)->get();
    if($existing_article->isEmpty())
    {
      $website = file_get_contents($request_url);
      $specificTags = 0;
      $doc = new \DOMDocument();
      @$doc->loadHTML($website);
      $res['title'] = $doc->getElementsByTagName('title')->item(0)->nodeValue;
      $node = $doc->getElementsByTagName('p');
      $article = '';
      for($c = 0;$c<$node->length;$c++)
      {
        $article = $article .$node->item($c)->nodeValue;
      }
      foreach ($doc->getElementsByTagName('meta') as $m){
          $tag = $m->getAttribute('name') ?: $m->getAttribute('property');
          if(in_array($tag,['description','keywords']) || strpos($tag,'og:')===0) $res[str_replace('og:','',$tag)] = $m->getAttribute('content');
      }
      $res['article'] = $article;
      $analyzer = new Analyzer();
      $output_text = $analyzer->getSentiment($article);
      $res['sentiment'] = $output_text;
      $tags = $res;
      $article = new Article();
      $article->title = $res['title'];
      $article->description = $res['description'];
      $save_url = $res['url'];
      $save_url = preg_replace("(^https?://)", "", $save_url);
      $article->url = $save_url;
      $article->image_url = $res['image'];
      $article->sentiment_analysis = $res['sentiment'];
      $article->article =$res['article'];
      $article->save();
      $article->already_exists = '0';
      $page = (object) array();
	$page->title = $article->title;
	$page->url = $article->url;
	$page->image_url = $article->url;
	$page->sentiment_analysis = $article->sentiment_analysis;
	return response()->json($page);
    }
    else
    {
      $existing_article->already_exists = '1';
      return response()->json($existing_article[0]);
    }
  }
}


